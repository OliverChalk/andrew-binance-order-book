"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const webSocket = require("ws");
const fetch = require("node-fetch");
const readline = require("readline");
class App {
    constructor(pair) {
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        this.pair = pair;
    }
    initialize() {
        this.url = "https://www.binance.com/api/v1/depth?symbol=" + this.pair + "&limit=1000";
        this.wsocket = "wss://stream.binance.com:9443/ws/" + this.pair.toLowerCase() + "@depth@100ms";
        this.settings = { method: "Get" };
        this.sortedBids = [];
        this.sortedAsks = [];
        this.bidArray = [];
        this.askArray = [];
        this.count = 0;
        this.isLock = false;
        this.deptJson = {};
        this.quantity = 0;
        this.askForQuantity();
        this.askPriceRecord = {};
        this.bidPriceRecord = {};
    }
    askForQuantity() {
        this.rl.question("Enter quantity: ", (quantity) => this.fetchForWSStream(quantity));
    }
    fetchForWSStream(quantity) {
        fetch(this.url, this.settings)
            .then(res => res.json())
            .then((json) => {
            this.deptJson = json;
            this.quantity = parseFloat(quantity);
            const ws = new webSocket(this.wsocket);
            ws.on('message', (data) => this.processWSocketStream(data, this.quantity));
        });
    }
    processWSocketStream(data, quantity) {
        let wsData = JSON.parse(data);
        if (this.deptJson.lastUpdateId >= wsData.u || this.deptJson.lastUpdateId <= wsData.U && !this.isLock) {
            fetch(this.url, this.settings)
                .then(res => res.json())
                .then((json) => {
                this.deptJson = json;
            });
        }
        else {
            this.isLock = true;
        }
        if (this.isLock) {
            let dataJson = JSON.parse(data);
            let bPrice = dataJson.b;
            let aPrice = dataJson.a;
            this.sortDownloadedData();
            this.bidArray = this.updatePriceArrayForBid(bPrice, this.bidArray);
            this.askArray = this.updatePriceArrayForAsk(aPrice, this.askArray);
            let resultBid = this.calculateWeight(quantity, this.bidArray);
            let priceOfBid = this.calculatePriceWWeight(resultBid[0], this.bidArray, resultBid[1]);
            let resultAsk = this.calculateWeight(quantity, this.askArray);
            let priceOfAsk = this.calculatePriceWWeight(resultAsk[0], this.askArray, resultAsk[1]);
            this.displayOutput(priceOfBid, priceOfAsk);
            if (priceOfAsk < priceOfBid) {
                //just to test if right condition is met everytime
                console.log("...Oops!!");
            }
            this.count++;
        }
    }
    displayOutput(priceOfBid, priceOfAsk) {
        process.stdout.write("\u001b[2J\u001b[0;0H");
        console.log("Buy/Bid" + "\t\t\t\t\t\t" + "Sell/Ask");
        console.log((Math.round(priceOfBid * 100) / 100) + "\t\t\t\t\t\t" + (Math.round(priceOfAsk * 100) / 100));
    }
    calculatePriceWWeight(totalOfWeight, bidArray, ctr3a) {
        let priceOfBid = 0;
        let ctr = 0;
        let ctr2a = 0;
        let testWeightA = 0;
        for (ctr2a = 0; ctr2a <= ctr3a; ctr2a++, ctr += parseFloat(bidArray[ctr2a][1])) {
            if (testWeightA < totalOfWeight) {
                testWeightA += parseFloat(bidArray[ctr2a][1]);
                priceOfBid += parseFloat(bidArray[ctr2a][0]) * (parseFloat(bidArray[ctr2a][1]) / totalOfWeight);
            }
        }
        if (testWeightA < totalOfWeight) {
            testWeightA += parseFloat(bidArray[ctr2a][1]);
            priceOfBid += parseFloat(bidArray[ctr2a][0]) * (parseFloat(bidArray[ctr2a][1]) / totalOfWeight);
        }
        return priceOfBid;
    }
    calculateWeight(quantity, bidArray) {
        let totalOfWeight = 0;
        let ctr = 0;
        let ctr3a = 0;
        for (ctr3a = 0; ctr <= quantity; ctr3a++, ctr += parseFloat(bidArray[ctr3a][1])) {
            totalOfWeight += parseFloat(bidArray[ctr3a][1]);
        }
        if (totalOfWeight < quantity)
            totalOfWeight += parseFloat(bidArray[ctr3a][1]);
        return [totalOfWeight, ctr3a];
    }
    updatePriceArrayForBid(bPrice, bidArray) {
        for (let ctr = 0; ctr < bPrice.length; ctr++) {
            this.bidPriceRecord[bPrice[ctr][0]] = bPrice[ctr][1];
            if (parseFloat(bPrice[ctr][1]) > 0) {
                if (parseFloat(bPrice[ctr][0]) > parseFloat(bidArray[0][0])) {
                    bidArray.unshift(bPrice[ctr]);
                }
                else if (parseFloat(bPrice[ctr][0]) <= parseFloat(bidArray[0][0]) && parseFloat(bPrice[ctr][0]) >= parseFloat(bidArray[bidArray.length - 1][0])) {
                    let index = this.binary_Search(bidArray, bPrice[ctr], false);
                    if (index[1]) {
                        bidArray.splice(index[0] + 1, 0, bPrice[ctr]);
                    }
                    else {
                        bidArray[index[0]] = bPrice[ctr];
                    }
                }
            }
            else if (parseFloat(bPrice[ctr][1]) == 0) {
                let index = this.binary_Search(bidArray, bPrice[ctr], false);
                if (!index[1]) {
                    bidArray[index[0]] = bPrice[ctr];
                }
            }
        }
        return bidArray;
    }
    updatePriceArrayForAsk(aPrice, askArray) {
        for (let ctr = 0; ctr < aPrice.length; ctr++) {
            this.askPriceRecord[aPrice[ctr][0]] = aPrice[ctr][1];
            let testLock = true;
            if (parseFloat(aPrice[ctr][1]) > 0) {
                if (parseFloat(aPrice[ctr][0]) < parseFloat(askArray[0][0])) {
                    askArray.unshift(aPrice[ctr]);
                }
                else if (parseFloat(aPrice[ctr][0]) >= parseFloat(askArray[0][0]) && parseFloat(aPrice[ctr][0]) <= parseFloat(askArray[askArray.length - 1][0])) {
                    let index = this.binary_Search(askArray, aPrice[ctr], true);
                    if (index[1]) {
                        askArray.splice(index[0] + 1, 0, aPrice[ctr]);
                    }
                    else {
                        askArray[index[0]] = aPrice[ctr];
                    }
                }
            }
            else if (parseFloat(aPrice[ctr][1]) == 0) {
                let index = this.binary_Search(askArray, aPrice[ctr], true);
                if (!index[1]) {
                    askArray[index[0]] = aPrice[ctr];
                    testLock = false;
                }
            }
        }
        return askArray;
    }
    sortDownloadedData() {
        if (this.count == 0) {
            this.sortedBids = this.deptJson.bids.sort(function (a, b) {
                return parseFloat(b[0]) - parseFloat(a[0]);
            });
            this.sortedAsks = this.deptJson.asks.sort(function (a, b) {
                return parseFloat(a[0]) - parseFloat(b[0]);
            });
            for (let ctr = 0; ctr < this.sortedBids.length; ctr++) {
                this.bidArray.push(this.sortedBids[ctr]);
            }
            for (let ctr = 0; ctr < this.sortedAsks.length; ctr++) {
                this.askArray.push(this.sortedAsks[ctr]);
            }
        }
        else {
            this.bidArray = this.bidArray.sort(function (a, b) {
                return parseFloat(b[0]) - parseFloat(a[0]);
            });
            this.askArray = this.askArray.sort(function (a, b) {
                return parseFloat(a[0]) - parseFloat(b[0]);
            });
        }
    }
    binary_Search(items, value, isAscending) {
        var firstIndex = 0, lastIndex = items.length - 1, middleIndex = Math.floor((lastIndex + firstIndex) / 2);
        let output;
        if (isAscending) {
            try {
                while (parseFloat(items[middleIndex][0]) != parseFloat(value[0]) && firstIndex < lastIndex) {
                    if (parseFloat(value[0]) < parseFloat(items[middleIndex][0])) {
                        lastIndex = middleIndex - 1;
                    }
                    else if (parseFloat(value[0]) > parseFloat(items[middleIndex][0])) {
                        firstIndex = middleIndex + 1;
                    }
                    middleIndex = Math.floor((lastIndex + firstIndex) / 2);
                    if (middleIndex == -1) {
                        middleIndex = 0;
                        break;
                    }
                }
            }
            catch (ex) {
                console.log();
            }
            output = (this.askPriceRecord[items[middleIndex][0]] == null) ? [middleIndex, true] : [middleIndex, false];
        }
        else {
            try {
                while (parseFloat(items[middleIndex][0]) != parseFloat(value[0]) && firstIndex < lastIndex) {
                    if (parseFloat(value[0]) > parseFloat(items[middleIndex][0])) {
                        lastIndex = middleIndex - 1;
                    }
                    else if (parseFloat(value[0]) < parseFloat(items[middleIndex][0])) {
                        firstIndex = middleIndex + 1;
                    }
                    middleIndex = Math.floor((lastIndex + firstIndex) / 2);
                    if (middleIndex == -1) {
                        middleIndex = 0;
                        break;
                    }
                }
            }
            catch (ex) {
                console.log();
            }
            output = (this.bidPriceRecord[items[middleIndex][0]] == null) ? [middleIndex, true] : [middleIndex, false];
        }
        return output;
    }
}
let app = new App("BTCUSDT");
app.initialize();
//# sourceMappingURL=app.js.map